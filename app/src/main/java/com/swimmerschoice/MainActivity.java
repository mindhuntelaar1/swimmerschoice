package com.swimmerschoice;

import android.Manifest;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import adapters.LocationsAdapter;
import dialog.MyDialog;
import entities.Adress;
import entities.Filter;
import entities.Location;
import entities.Quality;
import entities.SortMethod;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class MainActivity extends AppCompatActivity implements MyDialog.Communicator {


    public static ArrayList<Location> locations = new ArrayList<>();
    public static ArrayList<Location> filteredlocations = new ArrayList<>();
    public static ArrayList<Location> favorites = new ArrayList<>();
    public static Filter filter = new Filter(Integer.MAX_VALUE/1000, Quality.VERY_BAD, SortMethod.QUALITY);//Filter shows all results
    private Context context = this;
    private double latitude;
    private double longitude;
    private LocationsAdapter locationsAdapter;
    private LocationsAdapter favoritesAdapter;


    private LocationManager locationManager;
    private LocationListener locationListener;


    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    public static final String TAG = MainActivity.class.getSimpleName();


    private static final String PREFS_NAME = "com.swimmerschoice.UserPrefs";
    private static SharedPreferences.Editor editor;
    private static SharedPreferences settings;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            TextView spots_found = (TextView) findViewById(R.id.spots_found);
            saveFavorites();
            switch (item.getItemId()) {
                case R.id.navigation_main:
                    filteredlocations = filterList(locations);
                    locationsAdapter = new LocationsAdapter(context, filteredlocations);
                    ListView locationsView = (ListView) findViewById(R.id.locations_list);
                    locationsView.setAdapter(locationsAdapter);

                    locationsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> a, View v, int position,
                                                long id) {
                            //saveFavorites();
                            Location loc = (Location) a.getItemAtPosition(position);
                            Intent intent = new Intent();
                            intent.putExtra("location", loc);
                            intent.setClass(MainActivity.this, LocationDetailActivity.class);
                            startActivity(intent);
                        }
                    });
                    spots_found.setText(filteredlocations.size() + "");
                    return true;
                case R.id.navigation_favorites:
                    favoritesAdapter = new LocationsAdapter(context, favorites);
                    ListView locationsView2 = (ListView) findViewById(R.id.locations_list);
                    locationsView2.setAdapter(favoritesAdapter);

                    locationsView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> a, View v, int position,
                                                long id) {
                            //saveFavorites();
                            Location loc = (Location) a.getItemAtPosition(position);
                            Intent intent = new Intent();
                            intent.putExtra("location", loc);
                            intent.setClass(MainActivity.this, LocationDetailActivity.class);
                            startActivity(intent);
                        }
                    });
                    spots_found.setText(favorites.size() + "");
                    return true;
            }

            return false;
        }

    };

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 10:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocation();
                }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00C4CD")));

        //TODO
        loadFavorites();

        //TODO Feed nicht erreichbar




        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);


        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(android.location.Location location) {
                longitude = location.getLongitude();
                latitude = location.getLatitude();


                System.out.println(longitude + " " + latitude);
                // Update the distances

                for (Location l : locations) {
                    l.setDistance(distance(l.getLatitude(), latitude, l.getLongitude(), longitude, 0.0, 0.0));
                    System.out.println(l.getDistance());
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                requestPermissions(new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.INTERNET
                }, 10);

                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
        }
        getLocation();

        boolean network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        android.location.Location locationa;

        if(network_enabled){

            locationa = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            System.out.println(locationa == null);
            if(locationa!=null) {
                longitude = locationa.getLongitude();
                latitude = locationa.getLatitude();
            }

        }

        if (locations == null || locations.size() == 0) {
            try {
                loadJSON();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        filteredlocations = filterList(locations);

        locationsAdapter = new LocationsAdapter(this, filteredlocations);
        ListView locationsView = (ListView) findViewById(R.id.locations_list);
        locationsView.setAdapter(locationsAdapter);

        locationsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position,
                                    long id) {

                Location loc = (Location) a.getItemAtPosition(position);
                Intent intent = new Intent();
                intent.putExtra("location", loc);
                intent.setClass(MainActivity.this, LocationDetailActivity.class);
                startActivity(intent);
            }
        });

        TextView spots_found = (TextView) findViewById(R.id.spots_found);
        spots_found.setText(filteredlocations.size() + "");
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if (settings == null) {
            settings = this.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        }
        editor = settings.edit();

        if (getResources().getDisplayMetrics().widthPixels > getResources().getDisplayMetrics().
                heightPixels) {
            RelativeLayout rl = (RelativeLayout) findViewById(R.id.backgroundImageNumberLay);
            rl.setVisibility(View.GONE);
            rl = (RelativeLayout) findViewById(R.id.backgroundImageNumberLay);
            rl.setVisibility(View.GONE);
        }
    }

    private void getLocation() {
        //TODO FEHLER
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
           // added in order to remove the error message, the permission is being checked elsewhere
        }
        locationManager.requestLocationUpdates("gps", 5000, 1000, locationListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        MenuItem searchMenuItem = menu.findItem(R.id.search);
        if (searchMenuItem == null) {
            return false;
        }



        SearchView searchView = (SearchView) searchMenuItem.getActionView();
        if (searchView != null) {
            searchView.setQueryHint("search");
            searchView.setMaxWidth(2129960); // https://stackoverflow.com/questions/18063103/searchview-in-optionsmenu-not-full-width
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String s) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    MainActivity.this.onQueryTextChange(s);
                    return false;
                }
            });
        }



        return true;
    }


    void onQueryTextChange(String query) {
        locationsAdapter.getFilter().filter(query);
    }






    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.infoButton:
                // ProjectsActivity is my 'home' activity
                startActivity(new Intent(MainActivity.this,PopInfoActivity.class));
                return true;
            case R.id.action_filter:
                showFilterDialog(null);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showFilterDialog(View v) {

        FragmentManager manager = getFragmentManager();
        MyDialog myDialog = new MyDialog();
        myDialog.show(manager, "Filter-Dialog");
    }


    public void onDestroy(){
        super.onDestroy();
        locations.clear();
        saveFavorites();
        favorites.clear();
    }

    @Override
    public void onDialogMessage(int distance, Quality quality, SortMethod sortBy) {
        filter = new Filter(distance, quality, sortBy);
        filteredlocations = filterList(locations);

        locationsAdapter = new LocationsAdapter(context, filteredlocations);
        ListView locationsView = (ListView) findViewById(R.id.locations_list);
        locationsView.setAdapter(locationsAdapter);

        locationsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position,
                                    long id) {
                saveFavorites();
                Location loc = (Location)a.getItemAtPosition(position);
                Intent intent = new Intent();
                intent.putExtra("location", loc);
                intent.setClass(MainActivity.this, LocationDetailActivity.class);
                startActivity(intent);
            }
        });
    }


    private void loadFavorites() {
        Gson gson = new Gson();
        Type type = new TypeToken<List<Location>>(){}.getType();

        if(settings != null)
            favorites = gson.fromJson(settings.getString("favorites", ""), type);
    }

    private void saveFavorites() {
        Gson gson = new Gson();
        String json_loc = gson.toJson(favorites);

        editor.putString("favorites", json_loc);
        editor.commit();
    }
    public void loadJSON() throws JSONException, IOException {

        String URL = "https://www.ages.at/typo3temp/badegewaesser_db.json";

        JSONObject obj = null;
        try {
            obj = new JSONTask().execute(URL).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        JSONArray bundeslaender = obj.getJSONArray("BUNDESLAENDER");

        for(int k = 0; k < bundeslaender.length(); k ++) {
            JSONArray badegewaesser = bundeslaender.getJSONObject(k).getJSONArray("BADEGEWAESSER");
            for (int i = 0; i < badegewaesser.length(); i++) {
            //for (int i = 0; i < 150; i++) {
                String name = badegewaesser.getJSONObject(i).getString("BADEGEWAESSERNAME");
                JSONArray data = badegewaesser.getJSONObject(i).getJSONArray("MESSWERTE");
                double longitude = badegewaesser.getJSONObject(i).getDouble("LONGITUDE");
                double latitude = badegewaesser.getJSONObject(i).getDouble("LATITUDE");

                Adress adress = new Adress(badegewaesser.getJSONObject(i).getString("BADEGEWAESSERNAME"),
                        badegewaesser.getJSONObject(i).getString("BEZIRK"),
                        badegewaesser.getJSONObject(i).getString("PLZ_ORT"),
                        badegewaesser.getJSONObject(i).getString("STRASSE_NUMMER"),
                        badegewaesser.getJSONObject(i).getString("TELEFON"));

                //erster Datensatz ist der aktuellste
                double escherichia = data.getJSONObject(0).getDouble("E");
                double enterococci = data.getJSONObject(0).getDouble("E_C");
                double visible_depth = data.getJSONObject(0).getDouble("S");
                int water_temp = data.getJSONObject(0).getInt("W");
                Quality waterQuality = Quality.MEDIOCRE;

                switch (badegewaesser.getJSONObject(i).getString("WASSERQUALITAET_JAHR_VORIGES")) {
                    case "A":
                        waterQuality = Quality.EXCELLENT;
                        break;
                    case "B":
                        waterQuality = Quality.MEDIOCRE;
                        break;
                    case "C":
                        waterQuality = Quality.BAD;
                        break;
                }
                // TODO rating, koordinaten, surface, ph_value existieren im Datensatz nicht
                // Da wir den Datensatz ändern mussten, wird die luft temperatur berechnet
                Random r = new Random();
                int air_temp = r.nextInt((35 - 25) + 1) + 25;

                r = new Random();
                double randomValue = 1.0 + (5.0 - 1.0) * r.nextDouble();
                randomValue = Math.round(randomValue * 100);
                randomValue /= 100;

                Location l = (new Location(randomValue, waterQuality, null, air_temp, water_temp, null, visible_depth, escherichia, enterococci, adress,
                        badegewaesser.getJSONObject(i).getString("BADEGEWAESSERNAME"), longitude, latitude, visible_depth));

                l.setDistance(distance(l.getLatitude(), this.latitude, l.getLongitude(), this.longitude, 0.0, 0.0));

                locations.add(l);
            }
        }
    }

    public void favorite_add_clicked(View view) {

        ListView locationsView = (ListView) findViewById(R.id.locations_list);
        Location loc = (Location) locationsView.getItemAtPosition(locationsView.getPositionForView(view));
        Toast.makeText(this, loc.getName() + " added to Favorites", Toast.LENGTH_SHORT).show();
        favorites.add(loc);
        refresh_lists();

    }

    public void favorite_remove_clicked(View view) {

        ListView locationsView = (ListView) findViewById(R.id.locations_list);
        Location loc = (Location) locationsView.getItemAtPosition(locationsView.getPositionForView(view));
        Toast.makeText(this, loc.getName() + " removed from Favorites", Toast.LENGTH_SHORT).show();
        favorites.remove(loc);
        refresh_lists();
    }

    public void refresh_lists() {
        locationsAdapter.notifyDataSetChanged();
        if(favoritesAdapter != null)
            favoritesAdapter.notifyDataSetChanged();
    }


    private static double distance(double lat1, double lat2, double lon1,
                                  double lon2, double el1, double el2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }





    private static class JSONTask extends AsyncTask<String, String, JSONObject> {

        @Override
        protected JSONObject doInBackground(String... params) {
            try {
                return readJsonFromUrl(params[0]);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        private String readAll(Reader rd) throws IOException {
            StringBuilder sb = new StringBuilder();
            int cp;
            while ((cp = rd.read()) != -1) {
                sb.append((char) cp);
            }
            return sb.toString();
        }

        private JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
            InputStream is = new URL(url).openStream();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
                String jsonText = readAll(rd);
                JSONObject json = new JSONObject(jsonText);
                return json;
            } finally {
                is.close();
            }
        }
    }

    public ArrayList<Location> filterList(ArrayList<Location> list) {
        ArrayList<Location> ret = new ArrayList<Location>();
        for (Location location : list ) {

            if(( filter.getMaxDist() * 1000 >= location.getDistance() || filter.getMaxDist() == 0)
                    && filter.getMinQuality().getNumVal() >= location.getQuality().getNumVal() ) {
                ret.add(location);
            }
        }
        ret = sortList(ret);
        return ret;
    }

    public ArrayList<Location> sortList(ArrayList<Location> list) {

        if(filter.getSortMethod() == SortMethod.DISTANCE) {
            Collections.sort(list, new Comparator<Location>() {
                @Override
                public int compare(Location o1, Location o2) {
                    return Double.compare(o1.getDistance(), o2.getDistance());
                }
            });
        }
        if(filter.getSortMethod() == SortMethod.QUALITY) {
            Collections.sort(list, new Comparator<Location>() {
                @Override
                public int compare(Location o1, Location o2) {
                    return Integer.compare(o1.getQuality().getNumVal(), o2.getQuality().getNumVal());
                }
            });
        }
        if(filter.getSortMethod() == SortMethod.RATING) {
            Collections.sort(list, new Comparator<Location>() {
                @Override
                public int compare(Location o1, Location o2) {
                    return Double.compare(o2.getRating(), o1.getRating());
                }
            });
        }
        return list;
    }
}
