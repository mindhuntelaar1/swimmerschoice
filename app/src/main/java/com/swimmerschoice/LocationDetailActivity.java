package com.swimmerschoice;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.swimmerschoice.R;

import java.util.Locale;
import java.util.Random;

import entities.Location;
import entities.Quality;

public class LocationDetailActivity extends AppCompatActivity {

	Location location;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		android.support.v7.app.ActionBar bar = getSupportActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00C4CD")));
		setContentView(R.layout.activity_location_detail);
		location = (Location) getIntent().getSerializableExtra("location");

		TextView textView = (TextView) findViewById(R.id.name_val);
		textView.setText(location.getName());

		ImageView quality_drop = (ImageView)findViewById(R.id.quality_drop_val);

		int qualityValID = R.drawable.mediocredrop;
		if(location.getQuality() == Quality.BAD)
			qualityValID = R.drawable.verybaddrop;
		if(location.getQuality() == Quality.EXCELLENT)
			qualityValID = R.drawable.excellentdrop;

		quality_drop.setImageResource(qualityValID);


		textView = (TextView) findViewById(R.id.user_rating_val);
		textView.setText(location.getRating() + "");

		textView = (TextView) findViewById(R.id.air_temp_val);
		textView.setText(location.getAir_temp() + "");

		textView = (TextView) findViewById(R.id.water_temp_val);
		textView.setText(location.getWater_temp() + "");

		textView = (TextView) findViewById(R.id.visible_depth_val);
		textView.setText(location.getVisible_depth() + "");

		textView = (TextView) findViewById(R.id.escherichia_val);
		textView.setText(location.getEscherichia() + "");

		textView = (TextView) findViewById(R.id.enterococci_val);
		textView.setText(location.getEnterococci() + "");

		textView = (TextView) findViewById(R.id.adress_val);
		textView.setText(location.getAdress().getRegion() + ", " + location.getAdress().getStreet() + "\n" + location.getAdress().getZip());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.detail, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_filter) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void openGoogleMaps(View view) {

		String uri = "geo:" + location.getLatitude() + "," + location.getLongitude();
		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
		startActivity(intent);
	}
}
