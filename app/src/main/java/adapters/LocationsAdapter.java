package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.swimmerschoice.MainActivity;
import com.swimmerschoice.R;

import java.util.ArrayList;
import java.util.List;

import entities.Location;
import entities.Quality;

/**
 * Created by Amar on 29.04.2017.
 */

public class LocationsAdapter extends ArrayAdapter<Location> implements Filterable{


    private List<Location> originalData = null;
    private List<Location> filteredData = null;


    private LocationsFilter locationsFilter;


    public LocationsAdapter(Context context, ArrayList<Location> locations) {
        super(context, 0, locations);
        this.originalData = locations;
        this.filteredData = locations;
    }


    public int getCount()
    {
        return filteredData.size();
    }

    public View getView(int position, View convertView, ViewGroup parent){

        Location location = filteredData.get(position);

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_location, parent, false);
        }

        // Lookup view for data population

        /** will continue here **/

        TextView locationName = (TextView) convertView.findViewById(R.id.location_name);
        TextView locationRating = (TextView) convertView.findViewById(R.id.location_rating);
        TextView locationDistance = (TextView) convertView.findViewById(R.id.location_distance);

        ImageView favoriteFull = (ImageView) convertView.findViewById(R.id.favorite_full);
        ImageView favoriteEmpty = (ImageView) convertView.findViewById(R.id.favorite_empty);

        ImageView excellentDrop = (ImageView) convertView.findViewById(R.id.excellent_drop);
        ImageView goodDrop = (ImageView) convertView.findViewById(R.id.good_drop);
        ImageView mediocreDrop = (ImageView) convertView.findViewById(R.id.mediocre_drop);
        ImageView badDrop = (ImageView) convertView.findViewById(R.id.bad_drop);
        ImageView veryBadDrop = (ImageView) convertView.findViewById(R.id.very_bad_drop);


        /** name missing, waiting for feedback from nikola **/

        locationName.setText(location.getName().substring(0, Math.min(location.getName().length(), 20)));
        if(location.getName().length() >= 20)
            locationName.setText(locationName.getText() + "...");

        locationRating.setText("rating: " + location.getRating());


        locationDistance.setText(String.format("%.2f", location.getDistance()/1000) +" km");

        /** location quality will be done later */
        if(location.getQuality()==Quality.VERY_BAD){
            badDrop.setVisibility(View.GONE);
            mediocreDrop.setVisibility(View.GONE);
            goodDrop.setVisibility(View.GONE);
            excellentDrop.setVisibility(View.GONE);
            veryBadDrop.setVisibility(View.VISIBLE);
        } else
        if(location.getQuality()==Quality.BAD){
            veryBadDrop.setVisibility(View.GONE);
            badDrop.setVisibility(View.VISIBLE);
            mediocreDrop.setVisibility(View.GONE);
            goodDrop.setVisibility(View.GONE);
            excellentDrop.setVisibility(View.GONE);
        } else
        if(location.getQuality()==Quality.MEDIOCRE){
            veryBadDrop.setVisibility(View.GONE);
            badDrop.setVisibility(View.GONE);
            mediocreDrop.setVisibility(View.VISIBLE);
            goodDrop.setVisibility(View.GONE);
            excellentDrop.setVisibility(View.GONE);
        } else
        if(location.getQuality()==Quality.GOOD){
            veryBadDrop.setVisibility(View.GONE);
            badDrop.setVisibility(View.GONE);
            mediocreDrop.setVisibility(View.GONE);
            goodDrop.setVisibility(View.VISIBLE);
            excellentDrop.setVisibility(View.GONE);
        } else
        if(location.getQuality()==Quality.EXCELLENT){
            veryBadDrop.setVisibility(View.GONE);
            badDrop.setVisibility(View.GONE);
            mediocreDrop.setVisibility(View.GONE);
            goodDrop.setVisibility(View.GONE);
            excellentDrop.setVisibility(View.VISIBLE);
        }

        if(MainActivity.favorites.contains(location)){
            favoriteFull.setVisibility(View.VISIBLE);
            favoriteEmpty.setVisibility(View.GONE);
        } else {
            favoriteEmpty.setVisibility(View.VISIBLE);
            favoriteFull.setVisibility(View.GONE);
        }






        return convertView;
    }

    @Override
    public Filter getFilter() {
        if (locationsFilter == null)
            locationsFilter = new LocationsFilter();

        return locationsFilter;
    }


    private class LocationsFilter extends Filter {



        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint == null || constraint.length() == 0) {
                results.values = originalData;
                results.count = originalData.size();
            } else {
                List<Location> filteredResultsData = new ArrayList<Location>();
                for (Location l : originalData) {
                    if (l.getName().toUpperCase().contains(constraint.toString().toUpperCase()))
                        filteredResultsData.add(l);
                }
                results.values = filteredResultsData;
                results.count = filteredResultsData.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint,FilterResults results) {
            filteredData= (ArrayList<Location>) results.values;
            notifyDataSetChanged();
        }

    }
}
