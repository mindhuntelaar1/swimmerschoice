package dialog;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.swimmerschoice.R;

import entities.Quality;
import entities.SortMethod;


/**
 * Created by nikol on 05/06/2017.
 */

public class MyDialog extends DialogFragment implements View.OnClickListener {

    private Button ok_btn;
    private Spinner distance_filter;
    private Spinner quality_filter;
    private Spinner sort_filter;

    private Communicator communicator;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        communicator = (Communicator) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_filter, null);
        ok_btn = (Button) view.findViewById(R.id.filter_ok_button);
        ok_btn.setOnClickListener(this);
        distance_filter = (Spinner) view.findViewById(R.id.distance_filter);
        quality_filter = (Spinner) view.findViewById(R.id.quality_filter);
        sort_filter = (Spinner) view.findViewById(R.id.sort_filter);
        return view;
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.filter_ok_button) {
            dismiss();

            String distance = distance_filter.getSelectedItem().toString();
            distance = distance.replaceAll("km", "");
            Quality quality = Quality.valueOf(quality_filter.getSelectedItem().toString().toUpperCase().replaceAll(" ","_"));
            SortMethod sortBy = SortMethod.valueOf(sort_filter.getSelectedItem().toString().toUpperCase());

            communicator.onDialogMessage(Integer.parseInt(distance), quality, sortBy);
            Toast.makeText(getActivity(), "Filters set", Toast.LENGTH_SHORT).show();
        }
    }

    public interface Communicator {
        public void onDialogMessage(int distance, Quality quality, SortMethod sortBy);
    }
}
