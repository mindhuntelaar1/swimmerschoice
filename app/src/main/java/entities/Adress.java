package entities;

import java.io.Serializable;

public class Adress implements Serializable {

    private String name;
    private String region;
    private String zip;
    private String street;
    private String telNum;

    public Adress(String name, String region, String zip, String street, String telNum) {

        this.name = name;
        this.region = region;
        this.zip = zip;
        this.street = street;
        this.telNum = telNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getTelNum() {
        return telNum;
    }

    public void setTelNum(String telNum) {
        this.telNum = telNum;
    }
}
