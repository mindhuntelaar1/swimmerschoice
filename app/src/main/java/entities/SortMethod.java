package entities;

/**
 * Created by Nikola on 03/05/2017.
 */

public enum SortMethod {
        QUALITY, RATING, DISTANCE
}
