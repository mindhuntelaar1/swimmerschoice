package entities;

import java.io.Serializable;
import java.util.Objects;

public class Location implements Serializable{
	
	private double rating;
	private Quality quality;
	private String coordinates;
	private int air_temp;
	private int water_temp;
	private String surface;
	private double pH_value;
	private double escherichia;
	private double enterococci;
	private Adress adress;
	private String name;
	private double longitude;
	private double latitude;
	private double distance;
	private double visible_depth;
	
	public Location(double rating, Quality quality, String coordinates, int air_temp, int water_temp, String surface,
			double pH_value, double escherichia, double enterococci, Adress adress, String name, double longitude, double latitude, double visible_depth) {
		super();
        this.name=name;
		this.rating = rating;
		this.quality = quality;
		this.coordinates = coordinates;
		this.air_temp = air_temp;
		this.water_temp = water_temp;
		this.surface = surface;
		this.pH_value = pH_value;
		this.escherichia = escherichia;
		this.enterococci = enterococci;
		this.adress = adress;
		this.longitude = longitude;
		this.latitude = latitude;
		this.visible_depth = visible_depth;
	}

	public double getLongitude(){
		return longitude;
	}

	public double getLatitude(){
		return latitude;
	}

	public String getName(){
        return name;
    }

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	public Quality getQuality() {
		return quality;
	}

	public void setQuality(Quality quality) {
		this.quality = quality;
	}

	public String getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(String coordinates) {
		this.coordinates = coordinates;
	}

	public int getAir_temp() {
		return air_temp;
	}

	public void setAir_temp(int air_temp) {
		this.air_temp = air_temp;
	}

	public int getWater_temp() {
		return water_temp;
	}

	public void setWater_temp(int water_temp) {
		this.water_temp = water_temp;
	}

	public String getSurface() {
		return surface;
	}

	public void setSurface(String surface) {
		this.surface = surface;
	}

	public double getpH_value() {
		return pH_value;
	}

	public void setpH_value(double pH_value) {
		this.pH_value = pH_value;
	}

	public double getEscherichia() {
		return escherichia;
	}

	public void setEscherichia(double escherichia) {
		this.escherichia = escherichia;
	}

	public double getEnterococci() {
		return enterococci;
	}

	public void setEnterococci(double enterococci) {
		this.enterococci = enterococci;
	}

	public Adress getAdress() {
		return adress;
	}

	public void setAdress(Adress adress) {
		this.adress = adress;
	}

	public double getVisible_depth() {
		return visible_depth;
	}

	public void setVisible_depth(double visible_depth) {
		this.visible_depth = visible_depth;
	}

	/* fix this later */
	public double getDistance(){
		return distance;
	}

	public void setDistance(double distance){
		this.distance = distance;
	}


	public boolean equals(Object o){
		if(o == this) return true;
		if(!(o instanceof Location)) return false;
		Location l = (Location) o;
		/* will change it later to match the address instead of the name */
		return this.name.equals(l.name);
	}

	public int hashCode(){
		// change to address later
		return Objects.hash(name);
	}
}
