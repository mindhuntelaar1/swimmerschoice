package entities;

/**
 * Created by Nikola on 03/05/2017.
 */

public class Filter {

    private int maxDist;
    private Quality minQuality;
    private SortMethod sortMethod;

    public Filter(int maxDist, Quality minQuality, SortMethod sortMethod) {
        this.maxDist = maxDist;
        this.minQuality = minQuality;
        this.sortMethod = sortMethod;
    }

    public int getMaxDist() {
        return maxDist;
    }

    public void setMaxDist(int maxDist) {
        this.maxDist = maxDist;
    }

    public Quality getMinQuality() {
        return minQuality;
    }

    public void setMinQuality(Quality minQuality) {
        this.minQuality = minQuality;
    }

    public SortMethod getSortMethod() {
        return sortMethod;
    }

    public void setSortMethod(SortMethod sortMethod) {
        this.sortMethod = sortMethod;
    }
}
