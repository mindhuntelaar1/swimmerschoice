package entities;

import java.io.Serializable;

public enum Quality implements Serializable {
	EXCELLENT(1), GOOD(2), MEDIOCRE(3), BAD(4), VERY_BAD(5);

    private int numVal;

    Quality(int numVal) {
        this.numVal = numVal;
    }

    public int getNumVal() {
        return numVal;
    }
}
